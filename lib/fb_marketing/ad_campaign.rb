module FbMarketing
  class AdCampaign < Node
    
    register_attributes(
      raw: [
	:status,
	:name,
	# :buying_type,
	# :execution_options, # array
	:objective
      ],
      fields: [
	:fields
      ]
    )

    def create(options = {})
      self.id = "act_" + self.id + "/campaigns"
      update options
    end

    def read_by_ad_account(options = {})
      self.id = "act_" + self.id + "/campaigns"
      read options
    end

    # CONNECTIONS, append connection name to identifier
    def campaigns(options = {})
      self.id = self.id + "/campaigns"
      read options
    end
    def ads(options = {})
      self.id = self.id + "/ads"
      read options
    end
    def stats(options = {})
      self.id = self.id + "/stats"
      read options
    end

  end
end
